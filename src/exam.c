// NEWCAT : exécutable répliquant la fonction de l'instruction CAT
// permet de lire le contenu d'un fichier et de l'afficher à l'écran

// on importe les librairies nécessaires à notre exécutable
#include <stdlib.h>
#include <stdio.h>
// ici, la librairie string permettra d'utiliser le mot clé "EOF"
#include <string.h>

int main(int argc, char *argv[])
{
    // On initialise un pointeur de type "file" qui s'appelera "inputFile"
    FILE *inputFile;

    // On intialise deux variables pour les utiliser plus tard
    char i;
    char c;

    // On range dans notre pointeur inputFile le fichier que l'on passe en argument (argv[1]) de notre exécutable
    // Le "r" correspond ici à l'ouverture en lecture seule
    inputFile = fopen(argv[1], "r");

    // On utilise une condition pour vérifier si le nom du fichier a bien été renseigné
    // Si aucun nom de fichier n'est donné en argument, alors argc est forcément inférieur à 2
    if (argc < 2)
    {
        // Une fois dans la condition, on ouvre le fichier "stdin" (entrée par défaut du système) 
        // et on vérifie si il est égal à -1 (cela veut dire qu'il est vide)
        while ((c = fgetc(stdin)) != -1)
        {   
            // l'utilisateur peut entrer du texte
            // et on affiche à l'écran ce que l'utilisateur a tapé précédement
            printf("%c", c);
        }

        // saut de ligne pour la lisibilité
        printf("\n");
    }

    // on ouvre une condition "sinon si" vérifiant si le nom de fichier passé en argument existe
    else if (inputFile == NULL)
    {
        // si le fichier n'existe pas, alors on affiche un message d'erreur contenant le code erreur
        perror("Un problème est survenu : ");

        // on termine ensuite le programme directement sans aller plus loin
        abort();
    }

    // si aucun problème n'a été rencontré plus haut, alors on entre dans le "sinon"
    else
    {
        // indication pour la lisibilité
        printf("Ce fichier contient : \n");

        // on fait une boucle "do while", qui continue à itérer tant que la condition est respectée
        // on range dans i chaque caractère récupéré dans le fichier, 1 par 1 à chaque itération de la boucle
        while ((i = fgetc(inputFile))!= EOF)
        {   
            // puis on l'affiche à l'écran afin de montrer à l'utilisateur le contenu du fichier
            printf("%c", i);

            // la condition à respecter est "tant que i n'est pas la fin du fichier" : EOF = End Of File
            // c'est à dire tant qu'il reste quelque chose à afficher
        }

        // saut de ligne pour la lisibilité
        printf("\n");

        // on ferme le fichier car on a fini de l'utiliser
        fclose(inputFile);
    }

    // on renvoie un code de sortie équivalant à une réussite : 0
    // on peut vérifier que c'est bien 0 via la commande "echo $?" après l'exécution du code
    return EXIT_SUCCESS;
}