#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[])
{
   FILE * inputFile;
   FILE * outputFile;
   char ch;
  
    
    inputFile = fopen(argv[1], "r");
    
    outputFile = fopen("samples/output.txt", "w+");
  
    
    while((ch = getc(inputFile)) != EOF)
        putc(ch, outputFile);
  
    
    
    fclose(inputFile);
    fclose(outputFile);
    return EXIT_SUCCESS;
}
